Thank you for reading through the details of this project.

I decided to develop a small Android app to search and play videos. 

It will ask the user to type in a keyword to search for videos, and it will direct the user to Youtube site and show a list of videos. 

If the user has Youtube app installed , it pops up a message to ask whether the user wants to launch it in the Youtube app. 

Then the user choose the video they want to watch.
